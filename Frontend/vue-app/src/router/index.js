import { createRouter, createWebHistory } from 'vue-router'
import FactoryView from '../views/FactoryView.vue'
import AddFactory from '../views/AddFactory.vue';
import FactoryDetail from '../views/FactoryDetail.vue';
import Register from '../views/Register.vue';
import Login from '../views/Login.vue';
import Purchases from '../views/Purchases.vue'
import Cart from '../views/Cart.vue'
import ViewComments from'../views/ViewComments.vue'
import UserView from '../views/UserView.vue'
import FactoryReport from '../views/FactoryReport.vue'
import ViewWorkers from '../views/ViewWorkers.vue'
const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: FactoryView
    },
    {
      path: '/factories/:id',
      name: 'FactoryDetail',
      component: FactoryDetail
    },
    {
      path: '/add-factory',
      name: 'AddFactory',
      component: AddFactory
    },
    {
      path: '/register',
      name: 'Register',
      component: Register
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    }, {
      path: '/purchases',
      name: 'Purchases',
      component: Purchases // Dodata nova komponenta
    },{
      path: '/cart',
      name: 'Cart',
      component: Cart // Dodata nova komponenta
    },
    {
      path: '/factory/:factoryId/comments',
      name: 'ViewComments',
      component: ViewComments
    },
    {
      path: '/admin/users',
      name: 'UserView',
      component: UserView
    },
    {
      path: '/factory-report',
      name: 'FactoryReport',
      component: FactoryReport
    },
    {
      path: '/factory/:factoryId/workers',
      name: 'ViewWorkers',
      component: ViewWorkers
    }
  ]
})

export default router

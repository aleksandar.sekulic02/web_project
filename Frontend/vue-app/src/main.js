import './assets/main.css'

import { createApp } from 'vue'
import App from './App.vue'
import router from './router'

// Import Bootstrap and BootstrapVue CSS files
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue-next/dist/bootstrap-vue-next.css'

// Import BootstrapVueNext
import BootstrapVueNext from 'bootstrap-vue-next'

// Kreirajte aplikaciju
const app = createApp(App)

// Instalirajte BootstrapVueNext
app.use(BootstrapVueNext)

// Koristite router i montirajte aplikaciju
app.use(router)
app.mount('#app')

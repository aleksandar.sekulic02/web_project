const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const fs = require('fs'); // Dodato za rad sa fajlovima
const path = require('path');
const chocolateRouter = require('./Routes/ChocolateRoutes');
const factoryRouter = require('./Routes/FactoryRoutes');
const userRoutes = require('./Routes/UserRoutes');
const imageUpload = require('./Routes/imageUpload');
const cartRouter = require('./Routes/CartRoutes');
const purchasesRouter = require('./Routes/PurchaseRoutes');
const commentRouter = require('./Routes/CommentRoutes');

const app = express();
const port = process.env.PORT || 3000;

// Middleware za omogućavanje CORS-a
app.use(cors());

app.use(express.json()); // Ovaj middleware je potreban da bi `req.body` bio parsiran kao JSON

// Middleware za parsiranje JSON tela zahteva
//app.use(bodyParser.json());

// Middleware za logovanje zahteva (opciono)
app.use((req, res, next) => {
  console.log(`${req.method} ${req.url}`);
  next();
});

// Učitavanje sadržaja cart.json pri pokretanju servera
let cartsData;
try {
  const cartsFilePath = path.join(__dirname, 'data', 'cart.json');
  const cartsFileContent = fs.readFileSync(cartsFilePath, 'utf-8');
  cartsData = JSON.parse(cartsFileContent); // Parsiranje JSON sadržaja u JavaScript objekat
  console.log('cart.json učitan uspešno:', cartsData);
} catch (error) {
  console.error('Greška pri učitavanju cart.json:', error);
}

// Koristite cartsData u vašim rutama ili za druge potrebe
app.use('/upload', imageUpload);
app.use('/users', userRoutes);
app.use('/chocolates', chocolateRouter);
app.use('/purchases', purchasesRouter);
app.use('/factories', factoryRouter);
app.use('/carts', cartRouter);
app.use('/comments', commentRouter);

// Osnovna ruta za proveru statusa servera
app.get('/', (req, res) => {
  res.send('Server radi! Koristi /chocolates ili /factories ili /users ili /carts ili /purchases za API pristup.');
});

// Pokretanje servera
app.listen(port, () => {
  console.log(`Server je pokrenut na http://localhost:${port}`);
});

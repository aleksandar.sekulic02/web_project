const { readJsonFile, writeJsonFile } = require('../Utils/jsonHandler');
const Cart = require('../Models/Cart');

class CartDTO {
  constructor(filePath) {
    this.filePath = filePath;
  }

  async getAll() {
    return await readJsonFile(this.filePath);
  }

  async getCartByUser(username) {
    const carts = await this.getAll();
    return carts.find(cart => cart.user === username);
  }

  async add(cart) {
    try {
      const carts = await this.getAll();
      carts.push(cart);
      await writeJsonFile(this.filePath, carts);
    } catch (error) {
      console.error('Greška pri dodavanju nove korpe:', error);
      throw error;
    }
  }

  async updateCart(username, updatedCart) {
    try {
      let carts = await this.getAll();
      const index = carts.findIndex(cart => cart.user === username);

      if (index !== -1) {
        carts[index] = updatedCart;
        await writeJsonFile(this.filePath, carts);
      } else {
        throw new Error(`Korpa za korisnika ${username} nije pronađena.`);
      }
    } catch (error) {
      console.error('Greška pri ažuriranju korpe:', error);
      throw error;
    }
  }

  async delete(cartId) {
    try {
      let carts = await this.getAll();
      carts = carts.filter(cart => cart.id !== cartId);
      await writeJsonFile(this.filePath, carts);
    } catch (error) {
      console.error('Greška pri brisanju korpe:', error);
      throw error;
    }
  }

  async clearCart(username) {
    console.log(`Pozvana clearCart metoda za korisnika: ${username}`);
    try {
      let carts = await this.getAll();
      carts = carts.filter(cart => cart.user !== username);
      await writeJsonFile(this.filePath, carts);
      console.log(`Korpa za korisnika ${username} je uspešno obrisana.`);
    } catch (error) {
      console.error('Greška pri pražnjenju korpe:', error);
      throw error;
    }
  }
}

module.exports = CartDTO;


const fs = require('fs');
const { v4: uuidv4 } = require('uuid');

class FactoryDTO {
  constructor(factoryFilePath) {
    this.factoryFilePath = factoryFilePath;
  }

  async getAll() {
    return new Promise((resolve, reject) => {
      fs.readFile(this.factoryFilePath, 'utf8', (err, data) => {
        if (err) {
          return reject(err);
        }
        resolve(JSON.parse(data));
      });
    });
  }

  async getById(id) {
    return new Promise((resolve, reject) => {
      this.getAll().then(factories => {
        const factory = factories.find(factory => factory.id === id);
        if (factory) {
          resolve(factory);
        } else {
          reject(new Error('Factory not found'));
        }
      }).catch(reject);
    });
  }

  async add(factory) {
    return new Promise((resolve, reject) => {
      this.getAll().then(factories => {
        factory.id = uuidv4();
        factories.push(factory);
        fs.writeFile(this.factoryFilePath, JSON.stringify(factories, null, 2), 'utf8', (err) => {
          if (err) {
            return reject(err);
          }
          resolve(factory);
        });
      }).catch(reject);
    });
  }

  async update(id, updatedFactory) {
    return new Promise((resolve, reject) => {
      this.getAll().then(factories => {
        const index = factories.findIndex(factory => factory.id === id);
        if (index !== -1) {
          factories[index] = { ...factories[index], ...updatedFactory };
          fs.writeFile(this.factoryFilePath, JSON.stringify(factories, null, 2), 'utf8', (err) => {
            if (err) {
              return reject(err);
            }
            resolve(factories[index]);
          });
        } else {
          reject(new Error('Factory not found'));
        }
      }).catch(reject);
    });
  }


  async delete(id) {
    return new Promise((resolve, reject) => {
      this.getAll().then(factories => {
        const updatedFactories = factories.filter(factory => factory.id !== id);
        fs.writeFile(this.factoryFilePath, JSON.stringify(updatedFactories, null, 2), 'utf8', (err) => {
          if (err) {
            return reject(err);
          }
          resolve();
        });
      }).catch(reject);
    });
  }
}

module.exports = FactoryDTO;

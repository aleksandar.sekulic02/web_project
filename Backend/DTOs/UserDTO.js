const fs = require('fs').promises; // Importovanje modula fs sa promises podrškom
const { readJsonFile, writeJsonFile } = require('../Utils/jsonHandler');
const User = require('../Models/User');

class UserDTO {
  constructor(filePath) {
    this.filePath = filePath;
  }

  async getAll() {
    try {
      const users = await readJsonFile(this.filePath);
      console.log('Pročitani korisnici:', users);  // Dodajte log za korisnike
      return users.map(userData => new User(userData));
    } catch (error) {
      console.error('Greška pri čitanju korisnika:', error);  // Log greške
      throw error;
    }
  }

  async add(user) {
    try {
      const users = await this.getAll();
      console.log(users)

      users.push(user);
      console.log(users)
      for(let i  =0;i< users.length-1;i++){
        users[i]= users[i].username;
      }
      await writeJsonFile(this.filePath, users);
    } catch (error) {
      console.error('Greška pri dodavanju novog korisnika:', error);
      throw error;
    }
  }

  async update(userId, updatedUserData) {
    try {
      let users = await this.getAll();
      
    console.log(users);
      for(let i =0; i<users.length;i++){
        console.log(String(users[i].username.username));
        console.log(String(userId));
     if(String(users[i].username.username) === String(userId)){
      
     
        users[index] = { ...users[index], ...updatedUserData };
      
        for(let i  =0;i< users.length;i++){
          users[i]= users[i].username;
        }
        await writeJsonFile(this.filePath, users);
      } else {
        throw new Error('Korisnik sa datim ID-om nije pronađen.');
      }}
    } catch (error) {
      console.error('Greška pri ažuriranju korisnika:', error);
      throw error;
    }
  }
  async getAllManagers() {
    try {
      const users = await this.getAll();
      //console.log(user.username.role)
      const managers = users.filter(user => user.username.role === 'Menadzer' && user.username.chocolateFactory===null);
      return managers;
    } catch (error) {
      console.error('Greška pri dobavljanju menadžera:', error);
      throw error;
    }
  }


  async delete(userId) {
    try {
      let users = await this.getAll();
      users = users.filter(user => user.username !== userId);
      await writeJsonFile(this.filePath, users);
    } catch (error) {
      console.error('Greška pri brisanju korisnika:', error);
      throw error;
    }
  }

  async findByUsername(username) {
    try {
      const users = await this.getAll();
      for (let i = 0; i < users.length; i++) {
       
        if (String(users[i].username.username) === String(username)) {
          return users[i];
        }
      }
      return null; // Ako korisnik nije pronađen
    } catch (error) {
      console.error('Greška pri pronalaženju korisnika:', error);
      throw error;
    }
  }
  
  
}
module.exports = UserDTO;

const { readJsonFile, writeJsonFile } = require('../Utils/jsonHandler');

class ChocolateDTO {
  constructor(filePath) {
    this.filePath = filePath;
  }

  async getAll() {
    try {
      const factories = await readJsonFile(this.filePath);
      return factories; // Ovo vraća sve fabrike zajedno sa čokoladama
    } catch (error) {
      console.error('Greška pri čitanju čokolada:', error);
      throw error;
    }
  }

  // Dodajte ovu metodu za dohvatanje čokolade po ID-u
  async getById(chocolateId) {
    try {
      const data = await this.getAll(); // Pretpostavlja da getAll vraća sve čokolade
      for (const factory of data) {
        const chocolate = factory.chocolates.find(choc => choc.id === chocolateId);
        if (chocolate) {
          return chocolate;
        }
      }
      return null; // Vraća null ako čokolada nije pronađena
    } catch (error) {
      console.error('Greška pri dohvatanju čokolade po ID-u:', error);
      throw error;
    }
  }

  async add(chocolate, factoryId) {
    try {
        const factories = await readJsonFile(this.filePath); // Učitaj sve fabrike
        const factory = factories.find(f => f.id === factoryId); // Pronađi fabriku po ID-u

        if (!factory) {
            throw new Error('Fabrika sa datim ID-om nije pronađena.');
        }

        factory.chocolates.push(chocolate); // Dodaj novu čokoladu u fabriku
        await writeJsonFile(this.filePath, factories); // Zapiši ažurirane podatke nazad u fajl
    } catch (error) {
        console.error('Greška pri dodavanju nove čokolade:', error);
        throw error;
    }
  }

  async update(chocolateId, updatedChocolateData) {
    try {
      const chocolatesData = await this.getAll();  // Učitavanje chocolates.json
      const factoriesData = await readJsonFile('./Data/factories.json');  // Učitavanje factories.json
      
      let chocolateUpdated = false;
  
      // Ažuriramo čokoladu u chocolates.json
      for (const factory of chocolatesData) {
        const chocolateIndex = factory.chocolates.findIndex(choc => choc.id === chocolateId);
  
        if (chocolateIndex !== -1) {
          factory.chocolates[chocolateIndex] = { ...factory.chocolates[chocolateIndex], ...updatedChocolateData };
          chocolateUpdated = true;
          break;
        }
      }
  
      if (!chocolateUpdated) {
        throw new Error('Čokolada sa datim ID-om nije pronađena.');
      }
  
      // Ažuriramo čokoladu u factories.json
      for (const factory of factoriesData) {
        const chocolateIndex = factory.name.chocolates.findIndex(choc => choc.id === chocolateId);
  
        if (chocolateIndex !== -1) {
          factory.name.chocolates[chocolateIndex] = { ...factory.name.chocolates[chocolateIndex], ...updatedChocolateData };
          break;
        }
      }
  
      // Zapišemo ažurirane podatke nazad u oba fajla
      await writeJsonFile(this.filePath, chocolatesData);  // Upis u chocolates.json
      await writeJsonFile('./Data/factories.json', factoriesData);  // Upis u factories.json
  
    } catch (error) {
      console.error('Greška pri ažuriranju čokolade:', error);
      throw error;
    }
  }
  
  
  async delete(chocolateId) {
    try {
      const factories = await this.getAll();

      // Prolazak kroz sve fabrike
      for (const factory of factories) {
        // Filtriranje čokolada uz proveru da li je element `null`
        const initialLength = factory.chocolates.length;
        factory.chocolates = factory.chocolates.filter(choc => choc && choc.id !== chocolateId);

        // Ako je dužina niza čokolada smanjena, znači da je čokolada obrisana
        if (factory.chocolates.length < initialLength) {
          await writeJsonFile(this.filePath, factories);
          return;
        }
      }

      throw new Error('Čokolada sa datim ID-om nije pronađena.');
    } catch (error) {
      console.error('Greška pri brisanju čokolade:', error);
      throw error;
    }
  }
}

module.exports = ChocolateDTO;

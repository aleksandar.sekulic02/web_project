const { v4: uuidv4 } = require('uuid');
const { readJsonFile, writeJsonFile } = require('../Utils/jsonHandler');
const Purchase = require('../Models/Purchase');

class PurchaseDTO {
  constructor(filePath) {
    this.filePath = filePath;
  }

  async getAll() {
    try {
      const purchases = await readJsonFile(this.filePath);
      // Mapiramo sve podatke iz JSON-a na Purchase objekte
      return purchases.map(purchaseData => new Purchase({
        id: purchaseData.id,
        chocolates: purchaseData.chocolates,
        factory: purchaseData.factory,
        dateTime: purchaseData.dateTime,
        price: purchaseData.price,
        buyer: purchaseData.buyer,
        status: purchaseData.status,
        comment: purchaseData.comment,
        rating: purchaseData.rating
      }));
    } catch (error) {
      console.error('Greška pri čitanju svih kupovina:', error);
      throw error;
    }
  }
  

  async getPurchasesByCustomer(customerUsername) {
    try {
      const purchases = await this.getAll();
      // Filtrirajte kupovine na osnovu polja 'buyer' umesto 'chocolates.customer'
      return purchases.filter(purchase => purchase.buyer === customerUsername);
    } catch (error) {
      console.error('Greška pri dobijanju kupovina za kupca:', error);
      throw error;
    }
  }

  async getPurchasesByFactory(factoryId) {
    try {
      const purchases = await this.getAll();
      // Proverite ispravno polje za fabriku
      return purchases.filter(purchase => purchase.factory === factoryId);
    } catch (error) {
      console.error('Greška pri dobijanju kupovina za fabriku:', error);
      throw error;
    }
  }

  async add(purchase) {
    try {
      const purchases = await this.getAll();
      purchase.id = uuidv4();  // Generišite jedinstveni ID
      purchases.push(purchase);
      await writeJsonFile(this.filePath, purchases);
    } catch (error) {
      console.error('Greška pri dodavanju nove kupovine:', error);
      throw error;
    }
  }

  async update(purchaseId, updatedPurchaseData) {
    try {
      let purchases = await this.getAll();
      const index = purchases.findIndex(purchase => purchase.id === purchaseId);
      if (index !== -1) {
        purchases[index] = { ...purchases[index], ...updatedPurchaseData };
        await writeJsonFile(this.filePath, purchases);
      } else {
        throw new Error('Kupovina sa datim ID-om nije pronađena.');
      }
    } catch (error) {
      console.error('Greška pri ažuriranju kupovine:', error);
      throw error;
    }
  }

  async delete(purchaseId) {
    try {
      let purchases = await this.getAll();
      purchases = purchases.filter(purchase => purchase.id !== purchaseId);
      await writeJsonFile(this.filePath, purchases);
    } catch (error) {
      console.error('Greška pri brisanju kupovine:', error);
      throw error;
    }
  }
}

module.exports = PurchaseDTO;

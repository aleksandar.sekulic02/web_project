const { readJsonFile, writeJsonFile } = require('../Utils/jsonHandler');
const Comment = require('../Models/Comment');

class CommentDTO {
  constructor(filePath) {
    this.filePath = filePath;
  }

  async getAll() {
    try {
      const comments = await readJsonFile(this.filePath);
      return comments.map(commentData => new Comment(commentData));
    } catch (error) {
      console.error('Greška pri čitanju svih komentara:', error);
      throw error;
    }
  }

  async add(comment) {
    try {
      const comments = await this.getAll();
      comments.push(comment);
      await writeJsonFile(this.filePath, comments);
    } catch (error) {
      console.error('Greška pri dodavanju komentara:', error);
      throw error;
    }
  }

  async update(commentId, updatedCommentData) {
    try {
      let comments = await this.getAll();
      const index = comments.findIndex(comment => comment.id === commentId);
      if (index !== -1) {
        comments[index] = { ...comments[index], ...updatedCommentData };
        await writeJsonFile(this.filePath, comments);
      } else {
        throw new Error('Komentar sa datim ID-jem nije pronađen.');
      }
    } catch (error) {
      console.error('Greška pri ažuriranju komentara:', error);
      throw error;
    }
  }

  async delete(commentId) {
    try {
      let comments = await this.getAll();
      comments = comments.filter(comment => comment.id !== commentId);
      await writeJsonFile(this.filePath, comments);
    } catch (error) {
      console.error('Greška pri brisanju komentara:', error);
      throw error;
    }
  }
}

module.exports = CommentDTO;

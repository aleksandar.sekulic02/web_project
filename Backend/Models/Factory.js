const { v4: uuidv4 } = require('uuid');

// Definisanje klase Store
class Factory {
  constructor(name, chocolates, openingHours, status, location, logo, rating) {
    this.id = uuidv4(); // Generisanje jedinstvenog ID-a koristeći uuidv4
    this.name = name; // Naziv radnje
    this.chocolates = chocolates; // Čokolade koje su u ponudi (niz stringova)
    this.openingHours = openingHours; // Radno vreme objekta
    this.status = status; // Status (Radi ili Ne radi)
    this.location = location; // Lokacija
    this.logo = logo; // Logo (URL ili path do slike)
    this.rating = rating; // Ocena (0 do 5)
  }

  // Metoda za prikaz informacija o radnji
  displayInfo() {
    console.log(`ID: ${this.id}`);
    console.log(`Naziv: ${this.name}`);
    console.log(`Čokolade: ${this.chocolates.join(', ')}`);
    console.log(`Radno vreme: ${this.openingHours}`);
    console.log(`Status: ${this.status}`);
    console.log(`Lokacija: ${this.location}`);
    console.log(`Logo: ${this.logo}`);
    console.log(`Ocena: ${this.rating}`);
  }

  // Metoda za promenu statusa radnje
  updateStatus(newStatus) {
    if (['Radi', 'Ne radi'].includes(newStatus)) {
      this.status = newStatus;
      console.log(`Status je ažuriran na: ${this.status}`);
    } else {
      console.error('Nevažeći status. Koristite "Radi" ili "Ne radi".');
    }
  }

  // Metoda za ažuriranje ocene
  updateRating(newRating) {
    if (newRating >= 0 && newRating <= 5) {
      this.rating = newRating;
      console.log(`Ocena je ažurirana na: ${this.rating}`);
    } else {
      console.error('Nevažeća ocena. Mora biti između 0 i 5.');
    }
  }
}
module.exports = Factory;
const { v4: uuidv4 } = require('uuid');

class Cart {
    constructor(chocolate,user,totalPrice) {
      this.chocolates = chocolate; // Čokolade koje se nalaze u korpi
      this.user = user; // Korisnik koji je vlasnik korpe
      this.totalPrice = totalPrice; // Ukupna cena korpe, početno 0
    }
  
    addToCart(chocolate) {
      this.chocolates.push(chocolate);
      this.totalPrice += chocolate.price;
      console.log(`${chocolate.name} dodata u korpu.`);
    }
  
    displayCart() {
      console.log(`Korpa korisnika: ${this.user}`);
      console.log('Čokolade u korpi:');
      this.chocolates.forEach(chocolate => {
        console.log(`- ${chocolate.name}, Cena: ${chocolate.price} RSD`);
      });
      console.log(`Ukupna cena korpe: ${this.totalPrice} RSD`);
    }
  
    removeFromCart(chocolate) {
      const index = this.chocolates.indexOf(chocolate);
      if (index !== -1) {
        this.chocolates.splice(index, 1);
        this.totalPrice -= chocolate.price;
        console.log(`${chocolate.name} uklonjena iz korpe.`);
      } else {
        console.error(`${chocolate.name} nije pronađena u korpi.`);
      }
    }
  }
  
  module.exports = Cart;
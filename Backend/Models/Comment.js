class Comment {
    constructor({ id, username, factoryId, text, rating, status, purchaseId }) {
      this.id = id;
      this.username = username;
      this.factoryId = factoryId;
      this.text = text;
      this.rating = rating;
      this.status = status || 'Pending'; // Status može biti 'Pending', 'Approved', ili 'Rejected'
      this.purchaseId = purchaseId;
    }
  
    changeStatus(newStatus) {
      const validStatuses = ['Pending', 'Approved', 'Rejected'];
      if (validStatuses.includes(newStatus)) {
        this.status = newStatus;
      } else {
        throw new Error('Invalid status');
      }
    }
  }
  
module.exports = Comment;
  
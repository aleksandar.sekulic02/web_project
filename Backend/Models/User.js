const { v4: uuidv4 } = require('uuid');

class User {
    constructor(username, password, firstName, lastName, gender, dateOfBirth, role, purchases, cart, chocolateFactory, collectedPoints, customerType) {
        this.username = username; // Korisničko ime (jedinstveno)
        this.password = password; // Lozinka
        this.firstName = firstName; // Ime
        this.lastName = lastName; // Prezime
        this.gender = gender; // Pol
        this.dateOfBirth = dateOfBirth; // Datum rođenja
        this.role = role; // Uloga (Kupac, Radnik, Menadžer, Administrator)
        this.purchases = purchases; // Sve kupovine (ako je korisnik Kupac)
        this.cart = cart; // Korpa (ako je korisnik Kupac)
        this.chocolateFactory = chocolateFactory; // Fabrika čokolade objekat (ako je korisnik Menadžer)
        this.collectedPoints = collectedPoints; // Broj sakupljenih bodova (ako je korisnik Kupac)
        this.customerType = customerType; // Tip kupca
      }
  
    displayInfo() {
      console.log(`Korisničko ime: ${this.username}`);
      console.log(`Ime: ${this.firstName}`);
      console.log(`Prezime: ${this.lastName}`);
      console.log(`Pol: ${this.gender}`);
      console.log(`Datum rođenja: ${this.dateOfBirth}`);
      console.log(`Uloga: ${this.role}`);
      if (this.role === 'Kupac') {
        console.log(`Broj sakupljenih bodova: ${this.collectedPoints}`);
        if (this.customerType) {
          console.log('Tip kupca:');
          this.customerType.displayInfo();
        }
        console.log('Sve kupovine:');
        this.purchases.forEach(purchase => {
          purchase.displayInfo();
        });
        console.log('Korpa:');
        if (this.cart) {
          this.cart.displayCart();
        } else {
          console.log('Korpa je prazna.');
        }
      } else if (this.role === 'Menadžer') {
        console.log('Fabrika čokolade:');
        if (this.chocolateFactory) {
          this.chocolateFactory.displayInfo();
        } else {
          console.log('Nije dodeljena fabrika čokolade.');
        }
      }
    }
  
    updateRole(role) {
      this.role = role;
      console.log(`Uloga je ažurirana na: ${this.role}`);
    }
  
    addPurchase(purchase) {
      if (this.role === 'Kupac') {
        this.purchases.push(purchase);
      } else {
        console.log('Samo korisnici sa ulogom "Kupac" mogu dodavati kupovine.');
      }
    }
  
    addToCart(chocolate) {
      if (this.role === 'Kupac') {
        if (!this.cart) {
          this.cart = new Cart(this.username); // Kreiranje nove korpe ako ne postoji
        }
        this.cart.addToCart(chocolate);
      } else {
        console.log('Samo korisnici sa ulogom "Kupac" mogu dodavati u korpu.');
      }
    }
  
    updatePoints(points) {
      if (this.role === 'Kupac') {
        this.collectedPoints += points;
        console.log(`Broj sakupljenih bodova je ažuriran na: ${this.collectedPoints}`);
      } else {
        console.log('Samo korisnici sa ulogom "Kupac" mogu sakupljati bodove.');
      }
    }
  
    assignChocolateFactory(factory) {
      if (this.role === 'Menadžer') {
        this.chocolateFactory = factory;
        console.log(`Dodeljena fabrika čokolade: ${factory.name}`);
      } else {
        console.log('Samo korisnici sa ulogom "Menadžer" mogu dodeljivati fabriku čokolade.');
      }
    }
  
    assignCustomerType(customerType) {
      if (this.role === 'Kupac') {
        this.customerType = customerType;
        console.log(`Dodeljen tip kupca: ${customerType.name}`);
      } else {
        console.log('Samo korisnici sa ulogom "Kupac" mogu imati dodeljen tip kupca.');
      }
    }
  }
  
  module.exports = User;
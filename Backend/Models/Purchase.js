class Purchase {
  constructor({ id, chocolates, factory, dateTime, price, buyer, status, comment, rating }) {
    this.id = id;
    this.chocolates = chocolates;
    this.factory = factory;
    this.dateTime = dateTime;
    this.price = price;
    this.buyer = buyer;
    this.status = status;
    this.comment = comment;
    this.rating = rating;

    // Novo polje za čuvanje izračunatih poena
    this.calculatedPoints = this.calculatePoints();
  }

  // Metoda za prikaz informacija o kupovini
  displayInfo() {
    console.log(`ID: ${this.id}`);
    console.log('Čokolade koje su kupljene:');
    this.chocolates.forEach(chocolate => {
      console.log(`- ${chocolate.name}`);
    });
    console.log(`Fabrika: ${this.factory}`);
    console.log(`Datum i vreme kupovine: ${this.dateTime}`);
    console.log(`Cena: ${this.price} RSD`);
    console.log(`Kupac: ${this.buyer}`);
    console.log(`Status: ${this.status}`);
    console.log(`Comment: ${this.comment}`);
    console.log(`Rating: ${this.rating}`);
    console.log(`Poeni: ${this.calculatedPoints}`);
  }

  // Metoda za ažuriranje statusa
  updateStatus(newStatus) {
    const validStatuses = ['Obrada', 'Odobreno', 'Odbijeno', 'Otkazano'];
    if (validStatuses.includes(newStatus)) {
      this.status = newStatus;
      // Ažuriraj poene na osnovu novog statusa
      this.calculatedPoints = this.calculatePoints();
      console.log(`Status je ažuriran na: ${this.status}`);
    } else {
      console.error('Nevažeći status. Koristite "Obrada", "Odobreno", "Odbijeno" ili "Otkazano".');
    }
  }

  // Metoda za izračunavanje poena
  calculatePoints() {
    let points = 0;
  
    if (this.status === 'Obrada' || this.status ==='Odobreno') {
      points = (this.price / 1000) * 133;
    } else if (this.status === 'Otkazano' || this.status ==='Odbijeno' ) {
      points = (this.price / 1000) * 133 * 4 *(-1);
    }
  
    // Zaokruži na 2 decimale i konvertuj u broj
    return parseFloat(points.toFixed(2));
  }
}

module.exports = Purchase;

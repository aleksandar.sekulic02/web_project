const { v4: uuidv4 } = require('uuid');

// Definisanje klase Chocolate
class Chocolate {
  constructor(name, price, type, factory, category, weight, description, image, status, quantity) {
    this.id = uuidv4(); // Generisanje jedinstvenog ID-a koristeći uuidv4
    this.name = name; // Naziv čokolade
    this.price = price; // Cena čokolade
    this.type = type; // Vrsta (obična, za kuvanje, za piće,…)
    this.factory = factory; // Fabrika kojoj pripada
    this.category = category; // Tip (crna, mlečna, bela,...)
    this.weight = weight; // Gramaža
    this.description = description; // Opis čokolade
    this.image = image; // URL ili path do slike
    this.status = status; // Status (Ima na stanju, Nema na stanju)
    this.quantity = quantity; // Količina na stanju
  }

  // Metoda za prikaz informacija o čokoladi
  displayInfo() {
    console.log(`ID: ${this.id}`);
    console.log(`Naziv: ${this.name}`);
    console.log(`Cena: ${this.price} RSD`);
    console.log(`Vrsta: ${this.type}`);
    console.log(`Fabrika: ${this.factory}`);
    console.log(`Tip: ${this.category}`);
    console.log(`Gramaža: ${this.weight}g`);
    console.log(`Opis: ${this.description}`);
    console.log(`Slika: ${this.image}`);
    console.log(`Status: ${this.status}`);
    console.log(`Količina na stanju: ${this.quantity}`);
  }

  // Metoda za ažuriranje statusa čokolade
  updateStatus(newStatus) {
    if (['Ima na stanju', 'Nema na stanju'].includes(newStatus)) {
      this.status = newStatus;
      console.log(`Status je ažuriran na: ${this.status}`);
    } else {
      console.error('Nevažeći status. Koristite "Ima na stanju" ili "Nema na stanju".');
    }
  }}
  module.exports = Chocolate;
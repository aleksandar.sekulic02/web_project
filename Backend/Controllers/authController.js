const bcrypt = require('bcrypt');
const User = require('../Models/User'); // Pretpostavljam da ovde importuješ model korisnika

class AuthController {
  constructor(userDTO) {
    this.userDTO = userDTO;
  }

  async login(req, res) {
    try {
      const { username, password } = req.body;
      console.log(`Received login request for username: ${username}`);
      
      const users = await this.userDTO.getAll();
      console.log('Fetched users:', users);
      
      const user = users.find(user => user.username === username);
      console.log('Found user:', user);

      if (!user) {
        console.log('User not found');
        return res.status(401).send('Korisničko ime ili lozinka nisu ispravni.');
      }

      const isPasswordValid = await bcrypt.compare(password, user.password);
      console.log('Password validation result:', isPasswordValid);

      if (!isPasswordValid) {
        console.log('Invalid password');
        return res.status(401).send('Korisničko ime ili lozinka nisu ispravni.');
      }

      // Logovanje uspešno, možeš dodati logiku za token ili sesiju
      console.log('Login successful');
      res.status(200).send('Uspešno ste se prijavili.');
    } catch (error) {
      console.error('Error during login:', error);
      res.status(500).send('Greška prilikom logovanja korisnika.');
    }
  }
}

module.exports = AuthController;

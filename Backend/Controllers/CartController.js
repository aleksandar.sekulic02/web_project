const ChocolateDTO = require('../DTOs/ChocolateDTO');
const CartDTO = require('../DTOs/CartDTO');
const Cart = require('../Models/Cart');

const chocolateFilePath = './Data/chocolates.json'; // Putanja do fajla sa čokoladama
const cartFilePath = './Data/cart.json'; // Putanja do fajla sa korpama

// Funkcija za dobijanje cene čokolade na osnovu njenog ID-a
async function getChocolatePriceById(chocolateId, chocolateDTO) {
  try {
      const chocolates = await chocolateDTO.getAll(); // Dohvatanje svih čokolada iz fajla
      for (const factory of chocolates) {
          for (const choc of factory.chocolates) { // Prolazimo kroz sve čokolade u fabrici
              if (choc.id === chocolateId) {
                  return choc.price;
              }
          }
      }
      return 0; // Vraćamo 0 ako čokolada nije pronađena
  } catch (error) {
      console.error('Greška pri dohvatanju cene čokolade:', error);
      return 0;
  }
}

class CartController {
  constructor(cartFilePath) {
    this.cartDTO = new CartDTO(cartFilePath); // Inicijalizacija CartDTO-a sa putanjom do fajla sa korpama
    this.chocolateDTO = new ChocolateDTO(chocolateFilePath); // Inicijalizacija ChocolateDTO-a sa putanjom do fajla sa čokoladama
  }

  // Metoda za dobijanje svih korpi
  async getAll(req, res) {
    try {
      const carts = await this.cartDTO.getAll();
      res.json(carts);
    } catch (error) {
      res.status(500).send('Greška pri čitanju svih korpi.');
    }
  }

  // Metoda za dodavanje nove korpe
  async add(req, res) {
    try {
      const newCart = new Cart(req.body);
      await this.cartDTO.add(newCart);
      res.status(201).json(newCart);
    } catch (error) {
      res.status(500).send('Greška pri dodavanju nove korpe.');
    }
  }

  // Metoda za ažuriranje postojeće korpe
  async update(req, res) {
    try {
      const cartId = req.params.id;
      const updatedCartData = req.body;
      await this.cartDTO.update(cartId, updatedCartData);
      res.status(200).send('Korpa je ažurirana.');
    } catch (error) {
      res.status(500).send('Greška pri ažuriranju korpe.');
    }
  }

  // Metoda za brisanje korpe
  async delete(req, res) {
    try {
      const cartId = req.params.id;
      await this.cartDTO.delete(cartId);
      res.status(200).send('Korpa je obrisana.');
    } catch (error) {
      res.status(500).send('Greška pri brisanju korpe.');
    }
  }

  // Metoda za dodavanje čokolade u korpu
  async addToCart(req, res) {
    try {
      const { username, chocolateId, quantity } = req.body;
      console.log(`Dodavanje u korpu - Korisnik: ${username}, Čokolada ID: ${chocolateId}, Količina: ${quantity}`);
      let cart = await this.cartDTO.getCartByUser(username);

      // Koristimo chocolateDTO da dobijemo cenu čokolade
      const price = await getChocolatePriceById(chocolateId, this.chocolateDTO);

      if (price === 0) {
          return res.status(400).json({ message: 'Čokolada nije pronađena.' });
      }

      // Ako korpa ne postoji, kreiramo novu
      if (!cart) {
          const newCart = {
              chocolatesInCart: [{ chocolateId, quantity }],
              user: username,
              totalPrice: price * quantity
          };
          await this.cartDTO.add(newCart);
          return res.status(201).json({ message: 'Čokolada uspešno dodata u korpu.', cart: newCart });
      }

      // Ažuriranje postojeće korpe
      cart.chocolatesInCart.push({ chocolateId, quantity });
      cart.totalPrice += price * quantity; // Ažuriranje ukupne cene

      await this.cartDTO.updateCart(username, cart);

      return res.status(201).json({ message: 'Čokolada uspešno dodata u korpu.', cart });
    } catch (error) {
      console.error('Greška pri dodavanju u korpu:', error);
      res.status(500).json({ message: 'Greška pri dodavanju u korpu.' });
      console.log('Konačna korpa nakon dodavanja:', userCart);
    }
  }

  async getCartByUser(req, res) {
    try {
        const { username } = req.params;
        const cart = await this.cartDTO.getCartByUser(username);
        
        if (!cart) {
            return res.status(404).json({ message: 'Korpa nije pronađena za ovog korisnika.' });
        }

        res.json(cart);
    } catch (error) {
        console.error('Greška pri pregledu korpe:', error);
        res.status(500).send('Greška pri pregledu korpe.');
    }
  }


  // Metoda za ažuriranje količine čokolade u korpi
  async updateCartQuantity(req, res) {
    try {
        const { username, chocolateId, newQuantity } = req.body;

        console.log(`Ažuriranje korpe za korisnika: ${username}, Čokolada ID: ${chocolateId}, Nova količina: ${newQuantity}`);

        let cart = await this.cartDTO.getCartByUser(username);

        if (!cart) {
            console.log('Korpa nije pronađena za korisnika:', username);
            return res.status(404).json({ message: 'Korpa nije pronađena.' });
        }

        // Dohvati podatke o fabrici da proverimo zalihu
        const chocolates = await this.chocolateDTO.getAll();
        let foundChocolate = null;

        for (const factory of chocolates) {
            foundChocolate = factory.chocolates.find(choc => choc.id === chocolateId);
            if (foundChocolate) break;
        }

        if (!foundChocolate) {
            console.log(`Čokolada sa ID ${chocolateId} nije pronađena u fabrici.`);
            return res.status(404).json({ message: 'Čokolada nije pronađena.' });
        }

        // Ispisi dostupnu količinu čokolade u fabrici
        console.log(`Dostupna količina čokolade ID ${chocolateId} je ${foundChocolate.quantity}`);

        // Provera da li je tražena količina dostupna
        if (newQuantity > foundChocolate.quantity) {
            console.log(`Količina koju korisnik pokušava da doda je veća od dostupne (${newQuantity} > ${foundChocolate.quantity})`);
            return res.status(400).json({ message: `Maksimalna dostupna količina je ${foundChocolate.quantity}.` });
        }

        // Nastavljamo sa ažuriranjem korpe ako je količina validna
        const chocolateInCart = cart.chocolatesInCart.find(item => item.chocolateId === chocolateId);
        if (chocolateInCart) {
            chocolateInCart.quantity = newQuantity;
            cart.totalPrice = 0;

            for (const item of cart.chocolatesInCart) {
                const price = await getChocolatePriceById(item.chocolateId, this.chocolateDTO);
                cart.totalPrice += item.quantity * price;
            }

            await this.cartDTO.updateCart(username, cart);
            console.log(`Korpa korisnika ${username} uspešno ažurirana.`);
            return res.status(200).json({ message: 'Količina u korpi uspešno ažurirana.', cart });
        } else {
            console.log(`Čokolada sa ID ${chocolateId} nije pronađena u korpi korisnika ${username}.`);
            return res.status(404).json({ message: 'Čokolada nije pronađena u korpi.' });
        }
    } catch (error) {
        console.error('Greška pri ažuriranju korpe:', error);
        res.status(500).json({ message: 'Greška pri ažuriranju korpe.' });
    }
}
  
async removeFromCart(req, res) {
  try {
    const { username, chocolateId } = req.body;
    console.log(`Uklanjanje iz korpe - Korisnik: ${username}, Čokolada ID: ${chocolateId}`);
    
    let cart = await this.cartDTO.getCartByUser(username);
    
    if (!cart) {
      console.log('Korpa nije pronađena za korisnika:', username);
      return res.status(404).json({ message: 'Korpa nije pronađena.' });
    }

    // Filtriramo stavke u korpi da uklonimo odabranu čokoladu
    cart.chocolatesInCart = cart.chocolatesInCart.filter(item => item.chocolateId !== chocolateId);

    // Ponovo izračunavamo ukupnu cenu
    cart.totalPrice = 0;
    for (const item of cart.chocolatesInCart) {
      const price = await getChocolatePriceById(item.chocolateId, this.chocolateDTO);
      cart.totalPrice += item.quantity * price;
    }

    // Ažuriramo korpu nakon uklanjanja stavke
    await this.cartDTO.updateCart(username, cart);
    console.log(`Stavka uspešno uklonjena iz korpe korisnika ${username}.`);
    return res.status(200).json({ message: 'Stavka uspešno uklonjena.', cart });
  } catch (error) {
    console.error('Greška pri uklanjanju stavke iz korpe:', error);
    res.status(500).json({ message: 'Greška pri uklanjanju stavke iz korpe.' });
  }
}

  async clearCart(username) {
    try {
      let cart = await this.cartDTO.getCartByUser(username);

      if (!cart) {
        throw new Error(`Korpa nije pronađena za korisnika ${username}.`);
      }

      cart.chocolatesInCart = [];  // Isprazni listu čokolada
      cart.totalPrice = 0;         // Postavi totalPrice na 0

      await this.cartDTO.updateCart(username, cart);  // Ažuriraj korpu sa praznim podacima

      console.log(`Korpa za korisnika ${username} je uspešno obrisana.`);
    } catch (error) {
      console.error('Greška pri pražnjenju korpe:', error);
      throw error;
    }
  }
}

module.exports = CartController;

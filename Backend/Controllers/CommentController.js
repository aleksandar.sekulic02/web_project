const CommentDTO = require('../DTOs/CommentDTO');
const Comment = require('../Models/Comment');

class CommentController {
  constructor(commentFilePath) {
    this.commentDTO = new CommentDTO(commentFilePath);
  }

  async getAll(req, res) {
    try {
      const comments = await this.commentDTO.getAll();
      res.json(comments);
    } catch (error) {
      res.status(500).send('Error fetching comments');
    }
  }

  async add(req, res) {
    try {
      const newComment = new Comment(req.body);
      await this.commentDTO.add(newComment);
      res.status(201).json(newComment);
    } catch (error) {
      res.status(500).send('Error adding comment');
    }
  }

  async update(req, res) {
    try {
      const commentId = req.params.id;
      await this.commentDTO.update(commentId, req.body);
      res.status(200).send('Comment updated');
    } catch (error) {
      res.status(500).send('Error updating comment');
    }
  }

  async delete(req, res) {
    try {
      const commentId = req.params.id;
      await this.commentDTO.delete(commentId);
      res.status(200).send('Comment deleted');
    } catch (error) {
      res.status(500).send('Error deleting comment');
    }
  }

  async findById(req, res) {
    try {
      const commentId = req.params.id;
      const comment = await this.commentDTO.findById(commentId);
      if (comment) {
        res.json(comment);
      } else {
        res.status(404).send('Comment not found');
      }
    } catch (error) {
      res.status(500).send('Error fetching comment');
    }
  }

  async changeStatus(req, res) {
    try {
      const commentId = req.params.id;
      const { newStatus } = req.body;
      const comment = await this.commentDTO.findById(commentId);
      if (comment) {
        comment.changeStatus(newStatus);
        await this.commentDTO.update(commentId, comment);
        res.status(200).send('Comment status updated');
      } else {
        res.status(404).send('Comment not found');
      }
    } catch (error) {
      res.status(500).send('Error changing comment status');
    }
  }
}

module.exports = CommentController;

const UserDTO = require('../DTOs/UserDTO');
const User = require('../Models/User');
const userFilePath = './Data/user.json'; // Ova putanja mora biti ispravna

class UserController {
  constructor(userFilePath) {
    console.log(userFilePath)
    this.userDTO = new UserDTO(userFilePath);
  }

  async getAll(req, res) {
    try {
      const users = await this.userDTO.getAll();
  
      // Preuzimanje filtera, pretrage i sortiranja iz query parametara
      const { firstName, lastName, username, role, customerType, sortBy, sortOrder } = req.query;
  
      let filteredUsers = users;
  
      // Filtriranje prema imenu
      if (firstName) {
        filteredUsers = filteredUsers.filter(user => 
          user.username.firstName.toLowerCase().includes(firstName.toLowerCase())
        );
      }
  
      // Filtriranje prema prezimenu
      if (lastName) {
        filteredUsers = filteredUsers.filter(user => 
          user.username.lastName.toLowerCase().includes(lastName.toLowerCase())
        );
      }
  
      // Filtriranje prema korisničkom imenu
      if (username) {
        filteredUsers = filteredUsers.filter(user => 
          user.username.username.toLowerCase().includes(username.toLowerCase())
        );
      }
  
      // Filtriranje prema ulozi
      if (role) {
        filteredUsers = filteredUsers.filter(user => user.username.role === role);
      }
  
      // Filtriranje prema tipu korisnika
      if (customerType) {
        filteredUsers = filteredUsers.filter(user => user.username.customerType === customerType);
      }
  
      // Sortiranje
      if (sortBy) {
        filteredUsers = filteredUsers.sort((a, b) => {
          let sortResult = 0;
          if (sortBy === 'firstName') {
            sortResult = a.username.firstName.localeCompare(b.username.firstName);
          } else if (sortBy === 'lastName') {
            sortResult = a.username.lastName.localeCompare(b.username.lastName);
          } else if (sortBy === 'username') {
            sortResult = a.username.username.localeCompare(b.username.username);
          } else if (sortBy === 'collectedPoints') {
            sortResult = a.username.collectedPoints - b.username.collectedPoints;
          }
  
          return sortOrder === 'asc' ? sortResult : -sortResult;
        });
      }
  
      res.json(filteredUsers);
    } catch (error) {
      res.status(500).send('Greška pri čitanju svih korisnika.');
    }
  }
  

  async add(req, res) {
    try {
      const newUser = new User(req.body);
      await this.userDTO.add(newUser.username);
      res.status(201).json(newUser.username);
    } catch (error) {
      res.status(500).send('Greška pri dodavanju novog korisnika.');
    }
  }

  async update(req, res) {
    try {
      const userId = req.params.id;
      const updatedUserData = req.body;
      await this.userDTO.update(userId, updatedUserData);
      res.status(200).send('Korisnik je ažuriran.');
    } catch (error) {
      res.status(500).send('Greška pri ažuriranju korisnika.');
    }
  }

  async delete(req, res) {
    try {
      const userId = req.params.id;
      await this.userDTO.delete(userId);
      res.status(200).send('Korisnik je obrisan.');
    } catch (error) {
      res.status(500).send('Greška pri brisanju korisnika.');
    }
  }
  // Funkcija za dobavljanje svih menadžera koji nemaju dodeljenu fabriku
  async getAllManagers(req, res) {
    try {
      const users = await this.userDTO.getAll(); // Preuzima sve korisnike
      console.log(users); // Dodajte log za debagovanje
      const managers = users.filter(user => 
        user.username.role === 'Menadzer' && (user.username.chocolateFactory === null || user.username.chocolateFactory === "")
      );
      
      console.log(managers); // Dodajte log za debagovanje menadžera
      res.json(managers); // Vraća filtriranu listu menadžera
    } catch (error) {
      console.error('Greška pri dobavljanju menadžera:', error);
      res.status(500).send('Greška pri dobavljanju menadžera.');
    }
  }
  
  
  async login(req, res) {
    try {
      const { username, password } = req.body;
      const user = await  this.userDTO.findByUsername(username);
      console.log(user)
      if ( user.username.password === password) {
        console.log(user.username.role);
        res.status(200).send(user.username);
      } else {
        res.status(401).send('Neispravno korisničko ime ili lozinka.');
      }
    } catch (error) {
      res.status(500).send('Greška pri logovanju.');
    }
  }
  async assignFactoryToManager(req, res) {
    try {
      const username = req.params.username;
      const manager = await this.userDTO.findByUsername(username);
      
      if (true) {
        manager.username.chocolateFactory = "wadjawhdawdj";
        
        await this.userDTO.update(username, manager.username);
        res.status(200).send('Fabrika je dodeljena menadžeru.');
      } else {
        res.status(404).send('Menadžer nije pronađen.');
      }
    } catch (error) {
      res.status(500).send('Greška pri dodeljivanju fabrike menadžeru.');
    }
  }

}

module.exports = UserController;

const PurchaseDTO = require('../DTOs/PurchaseDTO');
const Purchase = require('../Models/Purchase');
const CartDTO = require('../DTOs/CartDTO');
const ChocolateDTO = require('../DTOs/ChocolateDTO');

class PurchaseController {
  constructor(purchaseFilePath, cartFilePath, chocolateFilePath) {
    this.purchaseDTO = new PurchaseDTO(purchaseFilePath);
    this.cartDTO = new CartDTO(cartFilePath);
    this.chocolateDTO = new ChocolateDTO(chocolateFilePath);
  }

  async getAll(req, res) {
    try {
      let purchases = await this.purchaseDTO.getAll();
  
      // Obogatite svaku kupovinu sa detaljima o čokoladama
      for (let purchase of purchases) {
        await enrichPurchaseWithChocolateDetails(purchase, this.chocolateDTO);
      }
  
      // Dodajemo filtriranje i sortiranje iz query parametara
      const { factoryName, priceMin, priceMax, dateFrom, dateTo, sortBy, sortOrder } = req.query;
  
      // Filtriranje prema nazivu fabrike
      if (factoryName) {
        purchases = purchases.filter(purchase =>
          purchase.factory.toLowerCase().includes(factoryName.toLowerCase())
        );
      }
  
      // Filtriranje prema ceni
      if (priceMin) {
        purchases = purchases.filter(purchase => purchase.price >= parseFloat(priceMin));
      }
      if (priceMax) {
        purchases = purchases.filter(purchase => purchase.price <= parseFloat(priceMax));
      }
  
      // Filtriranje prema datumu
      if (dateFrom) {
        purchases = purchases.filter(purchase => new Date(purchase.dateTime) >= new Date(dateFrom));
      }
      if (dateTo) {
        purchases = purchases.filter(purchase => new Date(purchase.dateTime) <= new Date(dateTo));
      }
  
      // Sortiranje
      if (sortBy) {
        purchases.sort((a, b) => {
          let sortVal = 0;
          if (sortBy === 'factoryName') {
            sortVal = a.factory.localeCompare(b.factory);
          } else if (sortBy === 'price') {
            sortVal = a.price - b.price;
          } else if (sortBy === 'date') {
            sortVal = new Date(a.dateTime) - new Date(b.dateTime);
          }
  
          return sortOrder === 'desc' ? -sortVal : sortVal;
        });
      }
  
      console.log('Filtrirani i sortirani podaci o kupovinama:', purchases); // Log filtriranih i sortiranih kupovina
      res.json(purchases);
    } catch (error) {
      res.status(500).send('Greška pri čitanju svih kupovina.');
    }
  }

  async getPurchasesByCustomer(req, res) {
    try {
      const customerUsername = req.params.username;
      let allPurchases = await this.purchaseDTO.getAll();
      const customerPurchases = allPurchases.filter(purchase => purchase.buyer === customerUsername);

      // Obogatite svaku kupovinu sa detaljima o čokoladama
      for (let purchase of customerPurchases) {
        await enrichPurchaseWithChocolateDetails(purchase, this.chocolateDTO);
      }

      res.json(customerPurchases);
    } catch (error) {
      console.error('Error fetching purchases for customer:', error);
      res.status(500).send('Error fetching purchases for customer');
    }
  }

  static async getPurchasesByFactory(req, res) {
    try {
      const factoryId = req.params.factoryId;
      const purchases = await this.purchaseDTO.getPurchasesByFactory(factoryId);
      res.json(purchases);
    } catch (error) {
      console.error('Error fetching purchases for factory:', error);
      res.status(500).send('Error fetching purchases for factory');
    }
  }

  async add(req, res) {
    try {
      const newPurchase = new Purchase(req.body);
      await this.purchaseDTO.add(newPurchase);
      res.status(201).json(newPurchase);
    } catch (error) {
      res.status(500).send('Greška pri dodavanju nove kupovine.');
    }
  }

  async update(req, res) {
    try {
      const purchaseId = req.params.id;
      const updatedPurchaseData = req.body;
      await this.purchaseDTO.update(purchaseId, updatedPurchaseData);
      res.status(200).send('Kupovina je ažurirana.');
    } catch (error) {
      res.status(500).send('Greška pri ažuriranju kupovine.');
    }
  }

  async delete(req, res) {
    try {
      const purchaseId = req.params.id;
      await this.purchaseDTO.delete(purchaseId);
      res.status(200).send('Kupovina je obrisana.');
    } catch (error) {
      res.status(500).send('Greška pri brisanju kupovine.');
    }
  }

  // Funkcija za obradu kupovine iz korpe
  async purchaseFromCart(req, res) {
    try {
      const { username, factoryId } = req.body;

      // Preuzmi korisničku korpu
      const cart = await this.cartDTO.getCartByUser(username);
      console.log('Korpa korisnika:', cart); // Log korpe
      if (!cart || cart.chocolatesInCart.length === 0) {
        return res.status(400).json({ message: 'Korpa je prazna.' });
      }

      // Validacija zaliha i kreiranje nove kupovine
      for (const cartItem of cart.chocolatesInCart) {
        console.log('Obrada stavke iz korpe:', cartItem); // Log za svaku stavku iz korpe
        const chocolate = await this.chocolateDTO.getById(cartItem.chocolateId);
        if (!chocolate || chocolate.quantity < cartItem.quantity) {
          return res.status(400).json({ message: `Nema dovoljno zaliha za čokoladu: ${chocolate ? chocolate.name : 'Nepoznata čokolada'}` });
        }
      }

      // Kreiranje nove kupovine
      const newPurchase = new Purchase({
        chocolates: cart.chocolatesInCart,
        factory: 'Bonnat Chocolates',
        dateTime: new Date().toISOString(),
        price: cart.totalPrice,
        buyer: username,
        status: 'Obrada',
        comment: '',
        rating: 0
      });

      console.log("Nova kupovina pre dodavanja:", newPurchase); // Log nove kupovine pre dodavanja
      await this.purchaseDTO.add(newPurchase);

      // Ažuriranje zaliha čokolade
      for (const cartItem of cart.chocolatesInCart) {
        const chocolate = await this.chocolateDTO.getById(cartItem.chocolateId);
        if (chocolate) {
          chocolate.quantity -= cartItem.quantity;
          await this.chocolateDTO.update(chocolate.id, chocolate);
        }
      }

      console.log('Nova kupovina:', newPurchase); // Log nove kupovine nakon dodavanja
      // Čišćenje korpe nakon kupovine
      await this.cartDTO.clearCart(username);

      res.status(201).json({ message: 'Kupovina uspešno obavljena.', purchase: newPurchase });
    } catch (error) {
      console.error('Greška pri obradi kupovine:', error);
      res.status(500).json({ message: 'Greška pri obradi kupovine.' });
    }
  }
}

// Funkcija za obogaćivanje podataka o čokoladama u kupovinama
async function enrichPurchaseWithChocolateDetails(purchase, chocolateDTO) {
  for (let chocolate of purchase.chocolates) {
    console.log("Obrađuje se čokolada:", chocolate); // Dodali smo log za ispis čokolade pre obogaćivanja

    // Pronađite fabriku koja sadrži čokoladu sa ovim ID-jem
    const factories = await chocolateDTO.getAll();
    let foundChocolate = null;

    for (let factory of factories) {
      foundChocolate = factory.chocolates.find(c => c.id === chocolate.chocolateId);
      if (foundChocolate) {
        // Ako smo pronašli čokoladu, dodajemo sve potrebne informacije
        chocolate.name = foundChocolate.name;
        chocolate.price = foundChocolate.price;
        chocolate.chocolateKind = foundChocolate.chocolateKind;
        chocolate.chocolateType = foundChocolate.chocolateType;
        chocolate.weight = foundChocolate.weight;
        chocolate.image = foundChocolate.image;
        break;
      }
    }

    if (!foundChocolate) {
      console.error(`Čokolada sa ID-jem ${chocolate.chocolateId} nije pronađena.`);
    } else {
      console.log("Čokolada uspešno obogaćena:", chocolate); // Log za uspešno obogaćene podatke
    }
  }
}

module.exports = PurchaseController;

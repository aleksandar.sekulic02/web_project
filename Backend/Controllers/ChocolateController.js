const ChocolateDTO = require('../DTOs/ChocolateDTO');
const Chocolate = require('../Models/Chocolate');

class ChocolateController {
  constructor(chocolateFilePath) {
    this.chocolateDTO = new ChocolateDTO(chocolateFilePath);
  }

  async getAll(req, res) {
    try {
      const chocolates = await this.chocolateDTO.getAll();
      res.json(chocolates);
    } catch (error) {
      res.status(500).send('Greška pri čitanju čokolada.');
    }
  }

  async add(req, res) {
    try {
        const { chocolate, factoryId } = req.body; // Dobij ID fabrike iz tela zahteva
        await this.chocolateDTO.add(chocolate, factoryId);
        res.status(201).json({ message: 'Čokolada uspešno dodata.' });
    } catch (error) {
        console.error('Greška pri dodavanju nove čokolade:', error);
        res.status(500).json({ message: 'Greška pri dodavanju nove čokolade.' });
    }
}


  async update(req, res) {
  try {
    const chocolateId = parseInt(req.params.id); // Preuzmi ID iz URL-a
    const updatedChocolateData = req.body; // Preuzmi podatke za ažuriranje iz tela zahteva
    await this.chocolateDTO.update(chocolateId, updatedChocolateData); // Pozovi DTO metodu za ažuriranje
    res.status(200).json({ message: 'Čokolada uspešno ažurirana.' });
  } catch (error) {
    console.error('Greška pri ažuriranju čokolade:', error);
    res.status(500).json({ message: 'Greška pri ažuriranju čokolade.' });
  }
}

  async delete(req, res) {
    try {
      const chocolateId = req.params.id;
      await this.chocolateDTO.delete(chocolateId);
      res.status(200).send('Čokolada je obrisana.');
    } catch (error) {
      res.status(500).send('Greška pri brisanju čokolade.');
    }
  }
}

module.exports = ChocolateController;

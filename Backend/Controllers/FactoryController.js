const FactoryDTO = require('../DTOs/FactoryDTO');
const Factory = require('../Models/Factory');

class FactoryController {
  constructor(factoryFilePath) {
    this.factoryDTO = new FactoryDTO(factoryFilePath);
  }

  // Get all factories
  // Get all factories with filtering and sorting
  async getAll(req, res) {
    try {
      let factories = await this.factoryDTO.getAll();

      const { name, chocolateName, location, averageRating, sortBy, sortOrder, onlyOpenFactories, chocolateKind, chocolateType } = req.query;

      // Filtriranje po imenu fabrike
      if (name) {
        factories = factories.filter(factory => 
          factory.name.name.toLowerCase().includes(name.toLowerCase())
        );
      }

      // Filtriranje po imenu čokolade
      if (chocolateName) {
        factories = factories.filter(factory => 
          factory.name.chocolates.some(chocolate => chocolate.name.toLowerCase().includes(chocolateName.toLowerCase()))
        );
      }

      // Filtriranje po lokaciji
      if (location) {
        factories = factories.filter(factory => 
          factory.name.location.city.toLowerCase().includes(location.toLowerCase()) ||
          factory.name.location.state.toLowerCase().includes(location.toLowerCase())
        );
      }

      // Filtriranje po oceni
      if (averageRating) {
        factories = factories.filter(factory => factory.name.rating >= parseFloat(averageRating));
      }

      // Filtriranje po statusu otvorenosti
      if (onlyOpenFactories === 'true') {
        factories = factories.filter(factory => factory.name.workStatus === 'Working');
      }

      // Filtriranje po vrsti i tipu čokolade
      if (chocolateKind) {
        factories = factories.filter(factory =>
          factory.chocolates.some(chocolate => chocolate.name.chocolateKind === chocolateKind)
        );
      }
      if (chocolateType) {
        factories = factories.filter(factory =>
          factory.chocolates.some(chocolate => chocolate.name.chocolateType === chocolateType)
        );
      }

      // Sortiranje
      if (sortBy) {
        factories = factories.sort((a, b) => {
          let sortVal = 0;
          if (sortBy === 'name') {
            sortVal = a.name.name.toLowerCase().localeCompare(b.name.name.toLowerCase());
          } else if (sortBy === 'location') {
            sortVal = a.location.city.toLowerCase().localeCompare(b.location.city.toLowerCase()) || 
                      a.location.state.toLowerCase().localeCompare(b.location.state.toLowerCase());
          } else if (sortBy === 'averageRating') {
            sortVal = a.rating - b.rating;
          }

          return sortOrder === 'desc' ? -sortVal : sortVal;
        });
      }

      res.json(factories);
    } catch (error) {
      res.status(500).send('Error fetching factories.');
    }
  }
  async getById(req, res) {
    try {
      const factoryId = req.params.id;
      const factory = await this.factoryDTO.getById(factoryId);
      if (factory) {
        res.json(factory);
      } else {
        res.status(404).send('Factory not found');
      }
    } catch (error) {
      res.status(500).send('Error fetching factory');
    }
  }

  // Add a new factory
  async add(req, res) {
    try {
      const newFactory = req.body;
      await this.factoryDTO.add(newFactory);
      res.status(201).json(newFactory);
    } catch (error) {
      res.status(500).send('Error adding factory.');
    }
  }

  // Update a factory by ID
  async update(req, res) {
    try {
      const factoryId = req.params.id;
      const updatedFactoryData = req.body;
      await this.factoryDTO.update(factoryId, updatedFactoryData);
      res.status(200).send('Factory updated.');
    } catch (error) {
      res.status(500).send('Error updating factory.');
    }
  }

  // Delete a factory by ID
  async delete(req, res) {
    try {
      const factoryId = req.params.id;
      await this.factoryDTO.delete(factoryId);
      res.status(200).send('Factory deleted.');
    } catch (error) {
      res.status(500).send('Error deleting factory.');
    }
  }
}

module.exports = FactoryController;

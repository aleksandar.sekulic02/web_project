const express = require('express');
const CartController = require('../Controllers/CartController');
const cartController = new CartController('./Data/cart.json'); // Ovdje se mora proslediti ispravna putanja

const router = express.Router();

// Rute za Cart CRUD operacije

// Brisanje korpe za korisnika
router.post('/clear-cart', async (req, res) => {
    console.log("Pozvan /clear-cart endpoint");
    try {
        const username = req.body.username;
        console.log(`Korpa korisnika ${username} će biti obrisana.`);
        await cartController.clearCart(username);
        res.status(200).send(`Korpa za korisnika ${username} je obrisana.`);
    } catch (error) {
        console.error('Greška pri pražnjenju korpe:', error);
        res.status(500).send('Greška pri pražnjenju korpe.');
    }
});

// Dodavanje čokolade u korpu
router.post('/add-to-cart', (req, res) => {
    console.log("Pozvan /add-to-cart endpoint");
    cartController.addToCart(req, res);
});

// Pregled korpe korisnika
router.get('/:username', (req, res) => {
    const username = req.params.username;
    console.log(`Pozvan /carts/${username} endpoint`);
    cartController.getCartByUser(req, res);
});

// Pregled svih korpi
router.get('/', (req, res) => {
    console.log("Pozvan /carts endpoint");
    cartController.getAll(req, res);
});

// Dodavanje nove korpe
router.post('/', (req, res) => {
    console.log("Pozvan POST /carts endpoint za dodavanje nove korpe");
    cartController.add(req, res);
});

// Ažuriranje korpe po ID-u
router.put('/:id', (req, res) => {
    const cartId = req.params.id;
    console.log(`Pozvan PUT /carts/${cartId} endpoint za ažuriranje korpe`);
    cartController.update(req, res);
});

// Uklanjanje stavke iz korpe
router.post('/remove-from-cart', (req, res) => {
    console.log("Pozvan /remove-from-cart endpoint");
    cartController.removeFromCart(req, res);
});

// Uklanjanje stavke iz korpe
router.post('/remove-from-cart', (req, res) => {
    console.log("Pozvan /remove-from-cart endpoint");
    cartController.removeFromCart(req, res);
});

router.post('/update-cart', (req, res) => {
    console.log("Pozvan /update-cart endpoint");
    console.log("Telo zahteva:", req.body);  // Dodajte ovaj log za prikaz tela zahteva
    cartController.updateCartQuantity(req, res);
  });

// Brisanje korpe po ID-u
router.delete('/:id', (req, res) => {
    const cartId = req.params.id;
    console.log(`Pozvan DELETE /carts/${cartId} endpoint za brisanje korpe`);
    cartController.delete(req, res);
});

module.exports = router;

const express = require('express');
const multer = require('multer');
const path = require('path');

const router = express.Router();
//C:\Users\Marko\OneDrive\Desktop\web_projekat\web_project\Frontend\vue-app\src\assets
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, path.join(__dirname, '../../Frontend/vue-app/src/assets')); // Putanja do vašeg assets direktorijuma na frontend-u
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + path.extname(file.originalname)); // Jedinstveno ime fajla
  }
});

const upload = multer({ storage: storage });

router.post('/upload', upload.single('file'), (req, res) => {
  if (!req.file) {
    return res.status(400).send('No file uploaded.');
  }
  res.status(200).json({ filePath: `assets/${req.file.filename}` });
});

module.exports = router;

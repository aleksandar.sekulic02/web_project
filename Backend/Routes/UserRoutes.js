const express = require('express');
const UserController = require('../Controllers/UserController');

const router = express.Router();
const userFilePath = './Data/user.json'; // Putanja do JSON fajla za korisnike
const userController = new UserController(userFilePath);

// Rute za User CRUD operacije

// Get all users
router.get('/', (req, res) => userController.getAll(req, res));

// Add a new user
router.post('/register', (req, res) => userController.add(req, res));

// Update a user by ID
router.put('/:id', (req, res) => userController.update(req, res));

// Delete a user by ID
router.delete('/:id', (req, res) => userController.delete(req, res));

// Login user
router.post('/login', (req, res) => userController.login(req, res));
router.get('/managers', (req, res) => {
    console.log('Pozvana ruta /users/managers');
    userController.getAllManagers(req, res);
  });
router.put('/:username/assign-factory', (req, res) => userController.assignFactoryToManager(req, res));
//router.get('/users/managers', userController.getAllManagers);

module.exports = router;

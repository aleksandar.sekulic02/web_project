const express = require('express');
const FactoryController = require('../Controllers/FactoryController');

const router = express.Router();
const factoryFilePath = './Data/factories.json'; // Putanja do JSON fajla za fabrike
const factoryController = new FactoryController(factoryFilePath);

// Rute za Factory CRUD operacije

// Get all factories
router.get('/', (req, res) => factoryController.getAll(req, res));

// Add a new factory
router.post('/', (req, res) => factoryController.add(req, res));

// Update a factory by ID
router.put('/:id', (req, res) => factoryController.update(req, res));
router.get('/:id', (req, res) => factoryController.getById(req, res));


// Delete a factory by ID
router.delete('/:id', (req, res) => factoryController.delete(req, res));

router.get('/search', (req, res) => factoryController.search(req, res));
module.exports = router;

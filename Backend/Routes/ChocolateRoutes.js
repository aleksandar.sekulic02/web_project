const express = require('express');
const ChocolateController = require('../Controllers/ChocolateController');

const router = express.Router();
const chocolateFilePath = './Data/chocolates.json'; // Putanja do JSON fajla za čokolade
const chocolateController = new ChocolateController(chocolateFilePath);

// Rute za Chocolate CRUD operacije

// Get all chocolates
router.get('/', (req, res) => chocolateController.getAll(req, res));

// Add a new chocolate
router.post('/add', (req, res) => chocolateController.add(req, res));

// Update a chocolate by ID
router.put('/:id', (req, res) => chocolateController.update(req, res));

// Delete a chocolate by ID
router.delete('/:id', (req, res) => chocolateController.delete(req, res));

module.exports = router;

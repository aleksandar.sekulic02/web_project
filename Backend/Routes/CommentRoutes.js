const express = require('express');
const router = express.Router();
const CommentController = require('../Controllers/CommentController');

const commentController = new CommentController('./Data/comments.json');

router.get('/', (req, res) => commentController.getAll(req, res));
router.post('/', (req, res) => commentController.add(req, res));
router.get('/:id', (req, res) => commentController.findById(req, res));
router.put('/:id', (req, res) => commentController.update(req, res));
router.delete('/:id', (req, res) => commentController.delete(req, res));
router.put('/:id/status', (req, res) => commentController.changeStatus(req, res));

module.exports = router;

const express = require('express');
const PurchaseController = require('../Controllers/PurchaseController');

const router = express.Router();

// Putanje do JSON fajlova
const purchaseFilePath = './Data/purchase.json'; // Putanja do JSON fajla za kupovine
const cartFilePath = './Data/cart.json'; // Putanja do JSON fajla za korpe
const chocolateFilePath = './Data/chocolates.json'; // Putanja do JSON fajla za čokolade

// Inicijalizacija PurchaseController-a sa potrebnim fajlovima
const purchaseController = new PurchaseController(purchaseFilePath, cartFilePath, chocolateFilePath);

// Rute za Purchase CRUD operacije
router.get('/', (req, res) => purchaseController.getAll(req, res));
router.post('/', (req, res) => purchaseController.add(req, res));
router.put('/:id', (req, res) => purchaseController.update(req, res));
router.delete('/:id', (req, res) => purchaseController.delete(req, res));
router.get('/customer/:username', (req, res) => purchaseController.getPurchasesByCustomer(req, res));
router.get('/factory/:factoryId', (req, res) => purchaseController.getPurchasesByFactory(req, res));

// Nova ruta za kupovinu iz korpe
router.post('/purchase', (req, res) => purchaseController.purchaseFromCart(req, res));

module.exports = router;
